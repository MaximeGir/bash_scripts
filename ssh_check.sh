#!/bin/bash

today=$(date +"%m-%d-%y")
service="ssh"

if (( ps fu | grep ssh | wc -l ) > 1)
then
echo -e "SSH service is RUNNING : $today " >> ~/logs/SSH_"$today"
rm -rf ~/1
else
todayDetail=$(date+"%T")
echo -e "SSH serve IS NOT RUNNING : $today at $todayDetail" >> ~/logs/SSH_"$today"
/etc/init.d/$service start
echo "Service restarted on $today at $todayDetail" >> ~/logs/SSH_"$today" 
fi
