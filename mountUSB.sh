#!/bin/sh

today=$(date +"%m-%d-%Y")
detail=$(date +"%T")

 if [ "$(ls -A ~/usbDrive)" ]; then
  echo "/dev/sda1 is already mounted (~/usbDrive) $today @ $detail" >> ~/logs/MOUNT_$today 
 else 
    echo "Re-mounting /dev/sda1...."
    mount -t vfat -o uid=maxime ,gid=maxime /dev/sda1 ~/usbDrive/ &> ~/logs/MOUNT_$today
 fi
