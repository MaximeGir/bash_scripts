#!/bin/bash
## Quickly checks if I have new gmail
 
echo -e "Checking for new messages...\n \c"
 
atomlines=`wget -T 3 -t 1 -q --secure-protocol=TLSv1 \
 --no-check-certificate \
 --user= emailAdd--password=password \
 https://mail.google.com/mail/feed/atom -O - \
 | wc -l`
 
echo -e "\r\c"
 
[ $atomlines -gt "8" ] \
 && echo -e "You have new gmail. \n \c" \
 || echo -e "No new gmail.  \n\c"

function gmail() {
curl -u email:password --silent "https://mail.google.com/mail/feed/atom" | tr -d '\n' | awk -F '<entry>' '{for (i=2; i<=NF; i++) {print $i}}' | perl -pe 's/^<title>(.*)<\/title><summary>(.*)<\/summary>.*?<name>(.*?)<\/name>.*$/\n$3\n\t$1\n\t$2/'
}
gmail
